package server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress; 

public class BroadCastServer extends Thread{

	private static     Snippet sn       = Snippet.getInstance();
	private DatagramSocket server;
	private  int port; 
	
	public BroadCastServer (int port) {
		this.port = port;
	}

	public void broadCastServer() throws Exception{
		byte[] receiveByte = new byte[1024];
		server = new DatagramSocket(port);
		server.setBroadcast(true);
		DatagramPacket receivePacket = new DatagramPacket(receiveByte, receiveByte.length);

		while(true){
			server.receive(receivePacket);
			byte[] send = InetAddress.getLocalHost().getHostAddress().getBytes();
			DatagramPacket sendPackage = new DatagramPacket(send, send.length, receivePacket.getAddress(), receivePacket.getPort());
			try {
				server.send(sendPackage);
			} catch (IOException e) {
				e.printStackTrace();
			}
			receivePacket.setLength(receiveByte.length);
		}
	}
	public void run() {
		try {
			broadCastServer();
		} catch (Exception e) {
			sn.popUpWindowTimedMessage("Could Not Start BroadCastServer on port " + port +". "+  e.getLocalizedMessage(), "Game Server: Fatal Error");
		}
	}
}
