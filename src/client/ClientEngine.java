package client;

import java.util.HashMap;
import java.util.Map;

import server.Level;
import server.Snippet;

public class ClientEngine  {
	private Snippet sn = Snippet.getInstance();
	private CommClient commClient;
	private static Map<String, Player> players; 
	private Screen screen;
	private String playerName;

	public ClientEngine(CommClient commClient) {
		this.commClient = commClient;
		players = new HashMap<>();
	}

	public void commandCenter(Command command) {
		Player player = findPlayer(command.getUser());
		if (player != null) {
			screen.addPlayerScore(command);
		}

		if (command.getCommand().equals("loginplease")) {
			playerName = sn.popUpWindowInput(command.getMessage(),commClient.getVERSION()+ " Login");
			commClient.msgToServer(new Command("userlogin", playerName));
			sn.log("PlayerName:"+playerName);
		}

		else if (command.getCommand().equals("newplayer")) {
			Player newPlayer = new Player(command.getUser());
			players.put(newPlayer.getName(), newPlayer);
			newPlayer.setXpos(command.getPosX());
			newPlayer.setYpos(command.getPosY());

			if (screen == null) {
				Level level = new Level( );
				screen = new Screen(level.getLevel(), newPlayer);
			}
			screen.drawPlayer(newPlayer);
			screen.addPlayerScore(command); // hertil
		
			screen.log(newPlayer.getName() + " entered the maze");
		}

		else if (command.getCommand().equals("logout")) {
			screen.remove_PlayerOldPos(player);
			players.remove(command.getUser());
			screen.log(command.getUser() +" left the game, don't know why?");
		} 

		else if (command.getCommand().equals("move")) {
			screen.movePlayer(player, command   );
		} 
		else if (command.getCommand().equals("shoot")) {
			screen.log(player.getName() + " shoots");
			screen.shootAnimation (player, command);
		}
		else if (command.getCommand().equals("dead")) {
			player.setAlive(false);
			screen.log(command.getMessage() ); // + " -> " +player.getName() + " alive:"+ player.isAlive() );
			screen.deadAnimation (player);
		}
		else if (command.getCommand().equals("hitwall")) {
			screen.turnPlayer (player, command.getDirection()) ;
			screen.log(player.getName() + " walked right into a wall!");
		} 
		else if ( command.getCommand().equals("kill")) {
			sn.shutDownSystem(10);;
		}
		else {
			sn.logMethod("command: '" + command + "' was not reconized" ) ;
		}
	}

	public Player findPlayer (String name) {
//		if (players.get(name) == null) {
//			sn.logMethod("did not find:"+ name);
//			sn.logMethod(getPlayers());
//		}
		return players.get(name);
	}

	public String getPlayers() {
		String result ="";
		for (String player: players.keySet() ) {
			result += player + ", ";
		}
		return result;
	}

	public String getPlayerName() {
		return playerName;
	}

}