package client;


import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
 
public class GamePlayerKeyInput implements KeyListener {
	private CommClient commClient;
	private Player player;

	public GamePlayerKeyInput(Player gamePlayer){
		this.player = gamePlayer;
		commClient = CommClient.getInstance();
	}

	public void keyPressed(KeyEvent ke) {
			String key = "";
			if (ke.getKeyCode() == KeyEvent.VK_UP)   {  key = ("up"); 	}
			if (ke.getKeyCode() == KeyEvent.VK_DOWN) { 	key = ("down");  }
			if (ke.getKeyCode() == KeyEvent.VK_LEFT) {  key = ("left");  }
			if (ke.getKeyCode() == KeyEvent.VK_RIGHT){  key = ("right"); }
			if (ke.getKeyCode() == KeyEvent.VK_SPACE){  key = ("shoot"); }

			if (key=="up" || key=="down" || key=="left" || key=="right") {
				commClient.msgToServer(new Command("move", player.getName(), key, -1,-1));
			}
			else if (key=="shoot") {
				commClient.msgToServer(new Command("shoot", player.getName())) ;
			} 
		 
	}

	public void keyReleased(KeyEvent ke) { } // nothing 
	public void keyTyped(KeyEvent arg0)  { } // nothing 	
}
