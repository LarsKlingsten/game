version 2014-10-19a

From Client to server 
-----------
userLogin#playerName
Logout#playerName
Move#playerName#Direction
Shoot#playerName#Direction

From Server to Client  
-----------
PleaseLogin#Login_message
newPlayer#playerName#PosX#PosY
kill
Logout#playerName
Move#playerName#Direction
Shoot#playerName#Direction

TODO: 
Update Score
Register Kill
Ensure that Players do not start on a wall or directly on anther player
thread to handle shooting amimation
thread to handle sounds