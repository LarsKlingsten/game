package client;

public class Player {
	private String name;
	private int xpos;
	private int ypos;
	
	private int xDir;
	private int yDir;
 	private int point;
	private String faceDirection;
	 private Screen screen;
	private boolean isAlive;

	public Player (String name  ) {
		this.name = name;
		this.point = 0;
		this.xpos = 0;
		this.ypos = 0;
		this.xDir = 0; 
		this.yDir = -1;  // = up
		this.faceDirection = "up"; 
		this.isAlive = true;
	}
	
	public String toString () {
		return name + " x:"+xpos+ " ypos:"+ ypos + " dir:"+getFaceDirection();
	}

	public Screen getScreen() {
		return screen;
	}

	public void setScreen(Screen screen) {
		this.screen = screen;
	}

	public int getPoint() {
		return point;
	}

	public int getXpos() {
		return xpos;
	}

	public void setXpos(int xpos) {
		this.xpos = xpos;
	}

	public int getYpos() {
		return ypos;
	}

	public void setYpos(int ypos) {
		this.ypos = ypos;
	}
	public String getFaceDirection() {
		return faceDirection;
	}

	public void setFaceDirection(String direction) {
		this.faceDirection = direction;
	}

	public String ToString () {
		return name + " " + point + " alive:"+isAlive();
	}
	public void addOnePoint() {
		point ++;
	}

	public void subOnePoint() {
		point --;
	}
	
	public void addPoint(int newPoints) {
		this.point += newPoints;
	}
	 

	public String getName () {
		return name;
	}

 
	public void setDirection(String playerDirection) {
		
		this.xDir = 0;
		this.yDir = 0;
		setFaceDirection(playerDirection);
	
		if (playerDirection.equals("up")) {
			yDir = -1;
		}
		else if (playerDirection.equals("down")) {
			yDir = 1;
		}
		else if (playerDirection.equals("right")) {
			xDir = 1;
		}
		else if (playerDirection.equals("left")) {
			xDir = -1;
		}
	}
	
	public int getCheckXPos() {
		return getXpos() + xDir;
	}
	public int getCheckYPos() {
		return getYpos() + yDir;
	}
	
	public void setPlayerNewPos( ) { 
		setXpos(getXpos() + xDir);
		setYpos(getYpos() + yDir);
	}
	
	public int getxDir() {
		return xDir;
	}

	public int getyDir() {
		return yDir;
	}
 
	public boolean isAlive() {
		return isAlive;
	}

	public void setAlive(boolean isAlive) {
		this.isAlive = isAlive;
	}
}