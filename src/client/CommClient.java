package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;

import server.Snippet;

public class CommClient { // extends Thread      {
	private static  CommClient instance;
	private static     Snippet sn   = Snippet.getInstance();
	private String[]     serverIp   = {"Note:ThisHostWillBeOverwritten" ,"localhost","10.10.138.157","10.0.0.30","194.239.205.157"}; 
	private final int    GAMESERVERPORT       = 11011;
	private final int    BROADCASTSERVERPORT  = 11012;
	private final String VERSION    = "Game Client v0.12";
	private boolean      isLogin    = false; 
	private static String DELIMITER = "#";
	private BufferedReader in;
	private PrintWriter out;
	private Socket     socket;
	private ClientEngine clientEngine ;

	public static CommClient getInstance () {
		if (instance == null) {
			instance = new CommClient();
		}
		return instance;
	}

	public String getVERSION() {
		return VERSION;
	}

	public static void main(String[] args)   {
		new CommClient();
	}

	public void msgToServer (Command command) {
		//sn.logMethod("command: " +command  );
		out.println (command);
	}
		
	private CommClient( ) {
		instance = this;
		clientEngine = new ClientEngine(this);

		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() { // handle shutdowns
			public void run() {
				try { 
					msgToServer(new Command ("logout", clientEngine.getPlayerName()));
				}
				catch (Exception e) {
					sn.logErrors(e.getLocalizedMessage());
				} // nothing 
			}
		}));
		
		runClient();
	}

	public void runClient() {
		try {
			connectToServer();
			listenForServerCommands();
		} catch (Exception e) {
			e.printStackTrace();
			sn.shutDownSystem(10);
		}
	}

	public void connectToServer () throws Exception { 
		boolean isConnected = false;
	
		// connect to broadcastServer -> note that overwrite first serverIp[0] is overwritten by Broadcastserver!!! 
		BroadCastClient client = new BroadCastClient(BROADCASTSERVERPORT);
		serverIp[0]  = client.broadCastClient();
		int loopThoughIpAddresses = 0;
		while (loopThoughIpAddresses < serverIp.length && !isConnected) {
			socket = new Socket();
			try {
				socket.connect(new InetSocketAddress(serverIp[loopThoughIpAddresses], GAMESERVERPORT), 1500);
				isConnected = true;
			} catch (IOException e) {
				sn.log2("Failed connection to: " +serverIp[loopThoughIpAddresses] +":"+ GAMESERVERPORT);
			}
			loopThoughIpAddresses++;
		}

		if (!isConnected) {
			sn.popUpWindowTimedMessage("Fatal Error! Can't continue as GameServer was not found.", VERSION);
			sn.shutDownSystem(10);
		}
		isLogin = true;
		in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		out = new PrintWriter(socket.getOutputStream(), true);
	} 

	public boolean isLogin() {
		return isLogin;
	}

	public void listenForServerCommands() throws IOException {

		Command command = new Command();
		while (true) {
			String input = in.readLine();
			if (input == null) {
				sn.shutDownSystem(10);
				return;
			}
			command.parseCommand(input, DELIMITER );
		 //	sn.logMethod(""+command);
			clientEngine.commandCenter(command);
		}
	}
}