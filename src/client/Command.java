package client;

import server.Snippet;

public class Command     {
	private String  command;
	private String  user; 
	private String  direction;
	private int     posX;
	private int     posY;
	private String  message;
	private int     score; 
	
	private Snippet sn = Snippet.getInstance();

	public Command(String command, String user) {
		this( command, user, "", -1, -1);
	}
	
	public Command(String commandStr, String user, String direction, Integer posX, Integer posY) {
		setCommand (commandStr);
		setUser(user);
		setDirection(direction);
		setPosX(posX);
		setPosY(posY);
		setMessage("n/a");
		 
	}

	public Command(String commandStr) {
		this(commandStr,""); 
	}
	public Command() {
		this("","","",-1,-1); 
	}
	
	@Override
	public String toString() {
		return command + "#"+user  +"#"+direction+ "#"+posX + "#"+ posY +"#"+score+ "#"+message ;
	}

	public String getCommand() {
		return command;
	}
	public void setCommand(String command) {
			this.command = command.toLowerCase().trim();
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	 
	public void parseCommand (String commandString, String delimitor ) {
		String [] commands = commandString.split(delimitor);
		try { 
		 //	sn.logMethod(commandString + " len:" + commands.length);
			setCommand  (commands [0]);
			setUser     (commands [1]);
			setDirection(commands [2]);
			setPosX     (Integer.parseInt(commands [3]));
			setPosY     (Integer.parseInt(commands [4]));
			setScore    (Integer.parseInt(commands [5]));
			setMessage    (commands [6]);  

		} catch (Exception e) {
			sn.logMethod(e.toString() + " cmd:"+command);
		}

		//System.out.println("Cammand TOString:"+ commandString + "_>"+ toString());;
	}



	public int getPosX() {
		return posX;
	}
	public void setPosX(int posX) {
		this.posX = posX;
	}
	public int getPosY() {
		return posY;
	}
	public void setPosY(int posY) {
		this.posY = posY;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}


}
