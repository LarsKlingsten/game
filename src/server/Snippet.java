package server;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Snippet {

	private static Snippet instance;

	private Date startDate = new Date();
	private Random  rndNumber = new Random(); 
	protected Snippet() {
		//Singleton - is/must be private
	}

	public static Snippet getInstance () {
		if (instance == null) {

			instance = new Snippet ();
		}
		return instance;
	}

	public void log (String LogText) {
		System.out.printf("%n" + datetimeToString(new Date()) +" "+  LogText); 
	}
	public void log2 (String LogText) {
		System.out.printf("%n" +LogText); 
	}

	public void logMethod (String LogText) {
		System.out.printf("%n<" +Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + LogText+"/>"); 
	}
	
	public   void logErrors (String LogText) {
		System.out.printf("%n%s method:%s error:%s", datetimeToString(new Date()),Thread.currentThread().getStackTrace()[2] , LogText); 


		for (int i=0; i<Thread.currentThread().getStackTrace().length;i++) {
			log(Thread.currentThread().getStackTrace()[i].toString());
		}

	}

	public  String datetimeToString (Date date) {
		SimpleDateFormat ft = new SimpleDateFormat ("yy.MM.dd HH:mm:ss");
		return (ft.format(date));
	} 

	public  String dateToHHMMString (Date date) {
		SimpleDateFormat ft = new SimpleDateFormat ("HH:mm");
		return (ft.format(date));
	} 


	public  String getTimeDiff (Date startDate, Date endDate) {
		return (endDate.getTime() - startDate.getTime()) / 1000.0 + " sec";
	}

	public  String getTimeLapse () {
		return (new Date().getTime() - this.startDate.getTime()) / 1000.0 + " sec";
	}
	public  double getTimeLapseInSeconds () {
		return (new Date().getTime() - this.startDate.getTime()) / 1000.0 ;
	}


	public String dateToString   (Date myDate ) {
		SimpleDateFormat myDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		return myDateFormat.format(myDate);
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartTimer() {
		this.startDate = new Date();
	}

	public String getTimeStamp() {
		SimpleDateFormat myDateFormat = new SimpleDateFormat("HH:mm");
		return myDateFormat.format(new Date());
	}
	public String getTimeStamp2() {
		SimpleDateFormat myDateFormat = new SimpleDateFormat("HH:mm:ss.SSS");
		return myDateFormat.format(new Date());
	}

	public String get1stHalf(String input, String limitor) {
		String command;
		int pos = input.indexOf(limitor);
		try {
			command = input.substring(0,pos) ;
		} 
		catch (Exception e) {
			command = input.trim();
		}
		return command; 
	}

	public String get2ndHalf(String input, String limitor) {
		String value = "";
		int pos = input.indexOf(limitor) +1;

		try {
			value = input.substring(pos).trim();
		} 
		catch (Exception e) {
			value = "";
		}
		return value; 
	}

	public  void shutDownSystem(int timeDelay) {
		try {
			Thread.sleep(timeDelay);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}   
		finally {
			log("shutting down");
			System.exit(0);
			
		}
	}

	public  void wait (int delayInMiliSecs) {
		try {
			Thread.sleep(delayInMiliSecs);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public  String getCommando(String input, String limitor) {
		String command;
		int pos = input.indexOf(limitor);
		try {
			command = input.substring(0,pos) ;
		} 
		catch (Exception e) {
			command = input.trim();
		}
		return command; 
	}

	public  String getPlayer(String input, String limitor) {
		String command;
		int pos = input.indexOf(limitor) +1;

		try {
			command = input.substring(pos).trim();
			int pos2 = command.indexOf(limitor);
			command = command.substring(pos, pos2);
		} 
		catch (Exception e) {
			command = input.trim();
		}
		return command; 
	}

	public   String getValueOne(String input, String limitor) {
		String command;
		int pos = input.lastIndexOf(limitor) +1;
		try {
			command = input.substring(pos) ;
		} 
		catch (Exception e) {
			command = input.trim();
		}
		return command; 
	}


	public  String popUpWindowInput ( String message, String title) {
		return  JOptionPane.showInputDialog(new JFrame() ,message, title,JOptionPane.PLAIN_MESSAGE);
	}

	public  void  popUpWindowMessage ( String message, String title) {
		JOptionPane.showConfirmDialog(new JFrame() ,message, title,JOptionPane.PLAIN_MESSAGE);
	}

	public  void  popUpWindowTimedMessage ( String message, String title) {
 			JOptionPane.showMessageDialog( null,message,title, JOptionPane.ERROR_MESSAGE);
	}
	
	public int getRandomInteger(int from, int to) {
		return rndNumber.nextInt(to-from) + from; 
	}
	
}



