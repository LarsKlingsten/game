package server;
import java.util.HashMap;
import java.util.Map;

import client.Command;
import client.Player;

public class ServerEngine {

	private Snippet sn ;  
	private Map<String,Player> players;  
	private String level[][];
	private static ServerEngine instance;
	private CommServer commServer;

	public static ServerEngine getInstance () {
		if (instance == null ){ 
			instance = new ServerEngine();
		}
		return instance;
	}

	private String wall = "w" ;
	private String empty = "e" ;


	private ServerEngine () {
		level = new Level().getLevel();
		players = new HashMap< >();
		sn  = Snippet.getInstance();
	}

	public void newPlayer(Command command) {
		Player player = new Player (command.getUser());  
		sn.log2("new Player:"+player);


		while (level[player.getXpos()][player.getYpos()] != empty) {
			player.setXpos(sn.getRandomInteger(1, 19));
			player.setYpos(sn.getRandomInteger(1, 19));
		}

		level[player.getXpos()][player.getYpos()] = player.getName(); // set players new position

		players.put(command.getUser(), player);
		command.setPosX(player.getXpos() );
		command.setPosY(player.getYpos());
		command.setScore(player.getPoint());
		commServer.writeAllClients(command);

	}


	public void logout (Command command, Player player) {
		level[player.getXpos()][player.getYpos()] = empty;
		players.remove(command.getUser());
		command.setCommand("logout");
		commServer.writeAllClients(command);
		ifGameOverThenRestart();
	}

	public void move (Command command, Player player) {
		if (!player.isAlive()) return;

		player.setDirection(command.getDirection());
		int x = player.getCheckXPos();
		int y = player.getCheckYPos();

		// player can't move
		if (!level[x][y].equals("e")) {
			player.subOnePoint();
			command.setCommand("hitWall");;
		} 
		// player move is okay
		else {
			level[player.getXpos()][player.getYpos()] = "e";              // remove players old position // "e" =empty
			player.setPlayerNewPos();
			level[player.getXpos()][player.getYpos()] = player.getName(); // set players new position
		}
		command.setScore(player.getPoint());
		command.setPosX(player.getXpos());
		command.setPosY(player.getYpos());
		commServer.writeAllClients(command);

	}

	public void shoot  (Command command, Player player) {
		if (!player.isAlive()) return;
		boolean isSearching  = true;
		player.subOnePoint();

		String  foundThis = "";
		int x = player.getXpos() + player.getxDir();
		int y  = player.getYpos() + player.getyDir(); 

		int foundX = -1;
		int foundY = -1;

		int i = 0;
		while (isSearching ) {

			if (player.getxDir()==0) {  // shot vertical
				if(level[x][y+i*player.getyDir()] != empty) {
					isSearching=false;
					foundX = x;
					foundY = y+i*player.getyDir();
					foundThis = level[foundX][foundY]; //;y+i*player.getyDir()];
				}  
			}
			else {
				if(level[x+i*player.getxDir()][y] != empty) {  
					isSearching=false;
					foundX = x+i*player.getxDir();
					foundY = y;
					foundThis = level[foundX][foundY];
				}
			}
			i++;
		}
		command.setPosX(foundX);
		command.setPosY(foundY);
		command.setScore(player.getPoint());
		commServer.writeAllClients(command);

		// An Enemy Was Shot!
		if (foundThis != wall) {
			player.addPoint(51); // 51 instead of 50 to compensate for the -1 point for making the shot.

			level[foundX][foundY] = empty; // erase player from board
			Player deadPlayer = players.get(foundThis);
			deadPlayer.addPoint(-50);
			deadPlayer.setAlive(false);
			command.setCommand("dead");
			command.setScore(deadPlayer.getPoint());
			command.setMessage(player.getName() + " shoots " + deadPlayer.getName() + ". "+ deadPlayer.getName()+ " is dead");
			command.setUser(deadPlayer.getName());
			commServer.writeAllClients(command);

			ifGameOverThenRestart();
		}
	}

	public void commandCenter (Command command) {
		// 	sn.logMethod("in:"+command   );
		Player player = players.get(command.getUser());

		if (command.getCommand().equals("move")) {
			move (command, player);
		}
		else if (command.getCommand().equals("logout")) {
			players.remove(command.getUser());
			logout (command,player );
		}
		else if (command.getCommand().equals("shoot")) {
			shoot (command, player);
		}
		else {
			sn.log2("Server: UnknownCommand: " +command + " "  +command.getCommand());

		}
	}

	public Map<String, Player> getPlayers() {
		return players;
	}

	public void printLevel () {  
		for (int j = 0; j < 20; j++) {
			for (int i = 0; i < 20; i++) {
				System.out.print(level[i][j]);
			}
			System.out.println();
		}
	}

	public CommServer getCommServer() {
		return commServer;
	}

	public void setCommServer(CommServer commServer) {
		this.commServer = commServer;
	}

	public void ifGameOverThenRestart(){
		int dead = 0;
		int alive = 0;
		for(Map.Entry<String, Player> playersInMap : players.entrySet()){
			if(playersInMap.getValue().isAlive() == false) {
				dead++;
			}
			else  	 		{
				alive++;;
			}
		}
		if (alive==1 && dead >= 1 ) {
			restartGame();
		}
	}

	public void restartGame () {
		level = new Level().getLevel();
		Command command = new Command();
		Player player;

		for (String p:  getPlayers().keySet())  {
			player =  players.get(p);
			player.setXpos(0);
			player.setYpos(0);
			player.setAlive(true);
			while (level[player.getXpos()][player.getYpos()] != empty) {
				player.setXpos(sn.getRandomInteger(1, 19));
				player.setYpos(sn.getRandomInteger(1, 19));
			}
			level[player.getXpos()][player.getYpos()] = player.getName(); // set players new position

			command.setPosX(player.getXpos() );
			command.setPosY(player.getYpos());
			command.setScore(player.getPoint());
			command.setUser(player.getName());
			command.setDirection(player.getFaceDirection());
			command.setCommand("move");
			commServer.writeAllClients(command);
		}
	}
}