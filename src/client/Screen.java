package client;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import server.Snippet;

public class Screen   {
	boolean soundPlaying = false;
	private Thread t1 = null;
	private final String VERSION = "v0.12";
	private static final String ImageAndSoundfolder =  "/image/";
	private static JLabel[][] labels = new JLabel[20][20];
	private String[][] level;

	private JFrame frame;
	private JPanel panelScore,  panelMain, panelAction;
	private JLabel textDebug ; 

	private static Map<String, JLabel> playersScores; 

	private Snippet sn = Snippet.getInstance();
	private Map<String, ImageIcon>  graphics; 
	private Map<String, AudioClip>  audio; 
	private String playerOnThisScreen;

	public Screen(String[][] level,Player player) {
		this.level = level;
		playersScores = new HashMap<>();

		playerOnThisScreen = player.getName();
		initIcons();
		initSound();

		frame = new JFrame( "Player: " + player.getName() + " "+ VERSION );
		frame.setBounds(300, 300, 800, 800);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(true);
		//javax.swing.JOptionPane.showMessageDialog(null, "" + this.getClass().getResource(""));
		panelMain = new JPanel();
		panelMain.setLayout(new GridLayout(20, 20, 0, 0));
		panelMain.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "KillerMaze", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		drawGameLevel(panelMain);
		frame.getContentPane().add(panelMain, BorderLayout.CENTER);

		panelAction = new JPanel();
		panelAction.setLayout(new GridLayout(1, 1, 0, 0));
		panelAction.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Action", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		frame.getContentPane().add(panelAction, BorderLayout.SOUTH);
		textDebug = new JLabel("bugs here");
		panelAction.add(textDebug);

		panelScore = new JPanel();
		panelScore.setLayout(new GridLayout(1, 3, 0, 0));
		panelScore.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Scores", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		frame.getContentPane().add(panelScore, BorderLayout.NORTH);

		frame.repaint();
		frame.setVisible(true);
		arrowKeyListener(player);
	}

	public void addPlayerScore (Command command)  {
		if (!playersScores.containsKey(command.getUser()) ) {
			playersScores.put(command.getUser(), new JLabel(command.getUser()));
			panelScore.add(playersScores.get(command.getUser()));
		}
		playersScores.get(command.getUser()).setText(command.getUser() +" " + command.getScore());;
	}

	public void removePlayerScore (Command command) {
		if (playersScores.containsKey(command.getUser()) ) {
			panelScore.remove(playersScores.get(command.getUser()));
			playersScores.remove(command.getUser());
		}
	}

	private void initSound() {

		audio =  new TreeMap <> ();
		audio.put("laser",   Applet.newAudioClip(Screen.class.getResource(ImageAndSoundfolder + "laserbeam.wav")));
		audio.put("odd",   Applet.newAudioClip(Screen.class.getResource(ImageAndSoundfolder + "odd.wav")));
		audio.put("gunshot",   Applet.newAudioClip(Screen.class.getResource(ImageAndSoundfolder + "gunshot.wav")));
		playSound("odd");
	}

	private void initIcons() {
		graphics =  new TreeMap <String, ImageIcon>();
		graphics.put("heroup",   new ImageIcon(Screen.class.getResource(ImageAndSoundfolder + "heroup.png")));
		graphics.put("herodown", new ImageIcon(Screen.class.getResource(ImageAndSoundfolder + "herodown.png")));
		graphics.put("heroleft", new ImageIcon(Screen.class.getResource(ImageAndSoundfolder + "heroleft.png")));
		graphics.put("heroright",new ImageIcon(Screen.class.getResource(ImageAndSoundfolder + "heroright.png")));
		graphics.put("floor",    new ImageIcon(Screen.class.getResource(ImageAndSoundfolder + "gulv2.png")));
		graphics.put("wall",     new ImageIcon(Screen.class.getResource(ImageAndSoundfolder + "mur1.png")));
		graphics.put("shootHorizon", new ImageIcon(Screen.class.getResource(ImageAndSoundfolder + "ildVandret.png")));
		graphics.put("shootVertical",new ImageIcon(Screen.class.getResource(ImageAndSoundfolder + "ildLodret.png")));
		graphics.put("enemyright",new ImageIcon(Screen.class.getResource(ImageAndSoundfolder + "enemyright.png")));
		graphics.put("enemyleft",new ImageIcon(Screen.class.getResource(ImageAndSoundfolder + "enemyleft.png")));
		graphics.put("enemyup",new ImageIcon(Screen.class.getResource(ImageAndSoundfolder + "enemyup.png")));
		graphics.put("enemydown",new ImageIcon(Screen.class.getResource(ImageAndSoundfolder + "enemydown.png")));
		
		
		graphics.put("gravestone",new ImageIcon(Screen.class.getResource(ImageAndSoundfolder + "gravestone.png")));
	}

	private void arrowKeyListener(Player player) {
		GamePlayerKeyInput ko = new GamePlayerKeyInput(player);
		frame.addKeyListener(ko);
	}

	public void remove_PlayerOldPos(Player player ) {
		labels[player.getXpos()][player.getYpos()].setIcon(graphics.get("floor"));
	}

	public void drawPlayer(Player player ) {
		if (playerOnThisScreen.equals(player.getName())) {
			labels[player.getXpos()][player.getYpos()].setIcon(graphics.get("hero"+player.getFaceDirection()));
		}
		else {
			labels[player.getXpos()][player.getYpos()].setIcon(graphics.get("enemy"+player.getFaceDirection()));
		}
	}

	public void log (String text) {
		textDebug.setText(text);
	}

	public void movePlayer(Player player, Command command) {
		player.setDirection(command.getDirection());
		remove_PlayerOldPos(player);

		player.setXpos(command.getPosX());
		player.setYpos(command.getPosY());
		drawPlayer(player);
	}

	public void turnPlayer (Player player, String playerDirection) {
		player.setDirection(playerDirection);
		drawPlayer(player);
	}

	public void deadAnimation(Player player) {
		labels[player.getXpos()][player.getYpos()].setIcon(graphics.get("gravestone"));
	}


	public void playSound (final String sound) {
		if (soundPlaying) {
			 t1.interrupt();
		}

		t1 = new Thread(new Runnable() {
			public void run()
			{
				soundPlaying = true;
				audio.get(sound).play();
				soundPlaying = false;        
			}});
		t1.start();
	}

	public void shootAnimation(Player player, Command command) {
		int x = player.getXpos()  + player.getxDir();
		int y  = player.getYpos()  + player.getyDir();;

		playSound("gunshot");

		for (int i = 0; i<20;i++) {
			if (player.getxDir()==0) {
				if( y+i*player.getyDir()  == command.getPosY()) return; 
				setGraphicLabel(x,y+i*player.getyDir(),"shootVertical");
			}
			else {
				if(x+i*player.getxDir() == command.getPosX()) return; 
				setGraphicLabel(x+i*player.getxDir(), y, "shootHorizon");	
			}
			sn.wait(5);
			if  (player.getxDir()==0){
				setGraphicLabel(x, y+i*player.getyDir(), "floor");
			} 
			else {
				setGraphicLabel(x+i*player.getxDir(),y,"floor");
			}
		}
	}


	private void setGraphicLabel(int x, int y, String icon) {
		try {
			labels[x][y].setIcon(graphics.get(icon));
		}
		catch (Exception E) {
			//			sn.logMethod(E.getLocalizedMessage() );
		}
	}

	public void drawGameLevel(JPanel panel ) {

		for (int j = 0; j < 20; j++) {
			for (int i = 0; i < 20; i++) {
				if (level[i][j].equalsIgnoreCase("w")) {	
					JLabel l = new JLabel(graphics.get("wall"));
					l.setSize(50, 50);
					panel.add(l);
					labels[i][j] = l;
				} else if (level[i][j].equalsIgnoreCase("e")) {
					JLabel l = new JLabel(graphics.get("floor"));
					l.setSize(50, 50);
					panel.add(l);
					labels[i][j] = l;
				}
			}
		}
	}
}