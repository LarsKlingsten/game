package client;


import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;

public class BroadCastClient {
	
	private int port; 
	public BroadCastClient (int port) {
		this.port = port;
	}
	
 	public String broadCastClient() throws Exception{
		DatagramSocket socket = new DatagramSocket();
		socket.setBroadcast(true);
		socket.setSoTimeout(2000);
		DatagramPacket dgram;
		int numberOfTimesTried = 0;
		String received = "";

		while(numberOfTimesTried < 3){
			try{
				byte[] b = new byte[1];
				dgram = new DatagramPacket(b, b.length, InetAddress.getByName("255.255.255.255"), port);
				socket.send(dgram);
				//System.out.println("Package have been sent, now waiting for responds");

				byte[] receive = new byte[1024];
				DatagramPacket receivePackage = new DatagramPacket(receive, receive.length);
				socket.receive(receivePackage);

				received = new String(receivePackage.getData()).trim();
				System.out.println("Found Server at " + received);
				numberOfTimesTried = 3;

			}catch(SocketTimeoutException ste){
				System.out.println("BroadCast! Searching for GameServer ...");
				numberOfTimesTried++;
			}
		}
		socket.close();
		return received;
	}
	 
}
