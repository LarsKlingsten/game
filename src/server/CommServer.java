package server;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import client.Command;
import client.Player;

public class CommServer {
	private final int  gameServerPort = 11011;
	private final int  broardCastServerPort = 11012;
	private static final String VERSION = "GameServer 0.12"; 
	private static Snippet sn                  = Snippet.getInstance();
	private static ServerEngine serverEngine;  // = new ServerEngine();
	private static Set<String> chatClientNames  = new HashSet<String>();
	private static Set<PrintWriter> chatClients = new HashSet<PrintWriter>();
	private static Map <String, PrintWriter>  chatSessions = new TreeMap <String, PrintWriter>() ;
	private static JFrame frame ;
	private JTextField    txtCommand ; 
	private static JTextArea connectedClients ; 
	private JTextArea serverDebug ; 
	private static String DELIMITER = "#";
	

	public static void main(String[] args)   {
		try {
			new CommServer().startServer();
		} catch (IOException e) {
			//nothing
		}
	}

	private CommServer() {
		serverEngine = ServerEngine.getInstance();
		serverEngine.setCommServer(this);
		frame = new JFrame(VERSION);
		txtCommand = new JTextField(40);
		txtCommand.setEditable(true);

		connectedClients = new JTextArea(8, 40);
		connectedClients.setEditable(false);
		serverDebug = new JTextArea(8, 40);
		frame.getContentPane().add(txtCommand, "North");
		frame.getContentPane().add(new JScrollPane(connectedClients), "Center");
		frame.getContentPane().add(new JScrollPane(serverDebug), "South");
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);

		txtCommand.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				log(txtCommand.getText());
				Command myCommand = new Command();
				myCommand.setCommand(txtCommand.getText());
				ServerThread.writeAllClients(myCommand);
				txtCommand.setText("");
			}
		});

		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			public void run() {
				ServerThread.writeAllClients(new Command("KILL"));
			}
		}));
	}

	private  void startServer () throws IOException {

		Thread t1 = new Thread(new BroadCastServer(broardCastServerPort));
		t1.start();

		log("BroadCastServer: Started on port " + broardCastServerPort);
		ServerSocket listener = null;
		try {

			listener = new ServerSocket(gameServerPort);
		}
		catch (Exception e){
			log("Fatal Error:Could not get handle on port:" + gameServerPort + " Now Shutting Down Server!");
			sn.shutDownSystem(5000);
		}
		log("GameServer: Started on port " + gameServerPort);

		try {
			while (true) {
				ServerThread st = new ServerThread(listener.accept());
				st.start();
				log("new Server Thread started id: " + st.getId());
			}
		} finally {
			listener.close();
		}
	}

	public void log (String text) {
		serverDebug.append(sn.getTimeStamp() + " " + text + "\n");
	}
	private static void addClientTextArea(String text) {
		connectedClients.append(sn.getTimeStamp()  + " " + text + "\n");
	}

	private static class ServerThread extends Thread {
		private boolean        isUserLoggedin = false; 
		private String         userName;
		private Player         player;
		private Socket         socket;
		private BufferedReader in;
		private PrintWriter    out;

		public ServerThread(Socket socket) {
			this.socket = socket;
		}

		public void run() {
			try {
				in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				out = new PrintWriter(socket.getOutputStream(), true);
				EstablishCredentials();
				boolean isConnected = true;
				serverEngine.newPlayer(new Command("newplayer",userName));
				publishAllPlayerPosition(userName);
				// listen to commands from the user
				while (isConnected) {
					String input = in.readLine();
					if (input == null) {
						return;
					}

					Command playerCommand = new Command();
					playerCommand.parseCommand(input, DELIMITER );

					if (playerCommand.getCommand().equals("logout")) {
						isConnected = false; // still run the below Command
					}

					serverEngine.commandCenter(playerCommand);


				}
			} catch (IOException e) {
				System.out.println(e);
			} finally {
				disconnectClient();
				sn.log(userName + " disconnected");
			}	
		}

		public void disconnectClient(){
			if (userName != null) {
				chatClientNames.remove(userName);
				CommServer.addClientTextArea(userName + " left"); 
			}
			if (out != null) {
				chatClients.remove(out);
			}
			try {
				socket.close();
			} catch (IOException e) {
				System.out.println("disconnectClient():" + e);
			}
			finally {
				//publishWhoIsOnline();
			}
		}

		private void EstablishCredentials() throws IOException{
			Command command = new Command(); 
			int loginAttempts = 0;
			while (!isUserLoggedin) {

				loginAttempts++;
				command.setCommand("LOGINPLEASE");
				command.setMessage("Welcome to KillerMaze. Please Enter your name");
				sendToClient(command)   ;

				String input = in.readLine();
				if (input == null) {
					return;
				}
				command.parseCommand(input, DELIMITER);

				if (command.getCommand().equals ("userlogin")) {
					loginAttempts--;
					CommServer.addClientTextArea(command.getUser()+ " connected from " + socket.getInetAddress() + ":" + socket.getPort());

					synchronized (chatClientNames) {
						if (!chatClientNames.contains(command.getUser())) {
							chatClientNames.add(command.getUser());
							userName = command.getUser();
							player = new Player(command.getUser());
							isUserLoggedin = true;
						}
						else {

							command.setCommand("LOGINPLEASE");
							command.setMessage(command.getUser() + " was already taken. Please Enter your name");
							sendToClient(command)  ;
						}
					}
				} 
				else {
					out.println("ERROR COMMAND UNKNOWN");
				}
				if (loginAttempts==3) {
					out.println("LOG TOO MANY TRIES. YOU HAVE BEEN DISCONNECTED.");
					out.println("KILL");
					disconnectClient();
					return;
				}
			}
			chatClients.add(out);
			chatSessions.put(command.getUser(), out);
		}

		public static  void writeAllClients(Command command) {
			for (PrintWriter writer : chatClients) {
				writer.println(command);
			}
		}

		public static  void publishAllPlayerPosition(String userName) {
			Player player;
			Map<String, Player> players= serverEngine.getPlayers();

			for (String p:  players.keySet())  {
				player =  players.get(p);
				if (player.isAlive()) {
					Command command = new Command("newplayer", player.getName());
					command.setPosX(player.getXpos());
					command.setPosY(player.getYpos());
					writeToClient(userName, command);
				}
			}
		}

		public  void sendToClient (Command command) {
			out.println(command);
		}

		public static void writeToClient (String userName, Command message) {
			PrintWriter pw = chatSessions.get(userName);
			if (pw!=null) {
				pw.println(message);
			}
		}
	}

	public void writeAllClients (Command command) {
		ServerThread.writeAllClients(command);
	}
	public void getAllPlayerPosition(String userName) {
		ServerThread.publishAllPlayerPosition ( userName);
	}
}